# https://adventofcode.com/2022/day/1
# Samuel Jaeschke 2022

param ([string]$inputFile = "day01-input.txt")

$elvesInventory = New-Object System.Collections.Generic.List[System.Collections.Generic.List[int]]
$elfInventory = New-Object System.Collections.Generic.List[int]

# Load
Get-Content $inputFile | foreach {
    if ($_.Trim().Length) {
        # Write-Host "Item: $([int]$_)"
        $elfInventory.Add($_)
    }
    elseif ($elfInventory.Count) {
        # Write-Host "$($elfInventory.Count) items, next elf"
        $elvesInventory.Add($elfInventory)
        $elfInventory = New-Object System.Collections.Generic.List[int]
    }
}

if ($elfInventory.Count) {
    # Write-Host "$($elfInventory.Count) items"
    $elvesInventory.Add($elfInventory)
}

Write-Host "$($elvesInventory.Count) elves"

# Part 1: Sum & Max
$elvesCalories = $elvesInventory | foreach {
    $_ | foreach {$total=0} {$total += $_} {$total}
}

$maxCalories = $elvesCalories | foreach {$max=0} {$max = [math]::Max($max, $_)} {$max}
Write-Host "Max calories: $maxCalories"

# Part 2: Top 3
$elvesCaloriesDesc = $elvesCalories | Sort-Object -Descending
$top3Calories = $elvesCaloriesDesc[0] + $elvesCaloriesDesc[1] + $elvesCaloriesDesc[2]
Write-Host "Top 3 calories: $top3calories"
