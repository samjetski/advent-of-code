# Advent of Code - 2022 Powershell

Samuel Jaeschke 2022

Challenges: https://adventofcode.com/2022

## Setup

Requirements:

- Powershell 7.3

Windows 11:

```powershell
winget install --id Microsoft.Powershell --source winget
```

Fedora 36:

```bash
# Register the Microsoft RedHat repository
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
curl https://packages.microsoft.com/config/rhel/8/prod.repo | sudo tee /etc/yum.repos.d/microsoft.repo
sudo dnf makecache

# Install PowerShell
sudo dnf install -y powershell

# Start PowerShell
pwsh
```

## Usage

Just run the script of the day, or pass an input filename. E.g.:

```powershell
.\day01-code.ps1 .\day01-example.txt
.\day01-code.ps1 .\day01-input.txt
```
