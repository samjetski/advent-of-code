# Advent of Code

Solutions to the [Advent of Code](https://adventofcode.com) challenge by [Samuel Jaeschke](https://gitlab.com/samjetski), in various languages.

- 2020 - Python
- 2021 - Python
- 2022 - Powershell

Special thanks to [Eric Wastl](http://was.tl/) for creating Advent of Code.
